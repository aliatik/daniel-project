import React from 'react';
import { Route, BrowserRouter } from 'react-router-dom'
import Landing from './Component/Landing'
// todo : fix here
// import Itinerary from './Component/Itinerary'
import Cities from './Component/Cities'
import CreateNewAccount from './Component/CreateNewAccount'
import Favorites from './Component/Favorites'
import Logout from './Component/Logout'
import LoginUsers from './Component/LoginUsers';
import './App.css';
class App extends React.PureComponent {
  
  render() {
    return (
      <BrowserRouter>
        <div>
        <Route exact path='/' component={Landing}/>
        <Route exact path='/cities' component={Cities}/>
        {/*<Route exact path='/cities/:ciudad' component={Itinerary}/>*/}
        <Route exact path='/favorites' component={Favorites}/>
        <Route exact path='/logout' component={Logout}/>
        <Route exact path='/login' component={LoginUsers}/>
        <Route exact path='/login/signup' component={CreateNewAccount}/>
        </div>
      </BrowserRouter>
    );
  }
}
export default App;


