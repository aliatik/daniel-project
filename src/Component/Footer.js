import React, { Component } from 'react';
import { MdHome } from "react-icons/md";
import { IoIosArrowBack } from "react-icons/io";
import 'bootstrap/dist/css/bootstrap.min.css';
import {  NavLink } from 'react-router-dom';
import './Footer.css'
import {Button} from 'react-bootstrap';
class Footer extends Component {
         
  
    constructor(props) {
      super(props);
      this.handleNext = this.handleNext.bind(this);
    }
    handleNext() {
      this.props.history.push('/page2');
    }
    handleBack() {
      this.props.history.push('/');
    }


    render() {
        
      return (
        <div className="footer">
         
          
            <div className="back">
            
                
              <Button onClick={this.handleBack} > <IoIosArrowBack  size="2rem"/> </Button>
            </div>
          

          <NavLink to='./' >
            <div className="home">
                <MdHome size="2rem"/>
            </div>
          </NavLink>
        </div>
      )
  
    }
  }

  export default Footer;
  
  
  
