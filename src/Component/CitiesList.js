import React, { Component } from 'react';
import { Link } from "react-router-dom"
import { connect } from 'react-redux';
import {getItineraries} from '../store/actions/itinerariesActions'


class CitiesList extends Component {

  
    componentDidMount () {
       this.props.getCities()
        
    }
    

    render() {
      
        let citiesFiltrados = this.props.cities.filter(city => city.name.toUpperCase().startsWith(this.props.search))
        console.log(citiesFiltrados)

        let serchcities= citiesFiltrados.map((city) => {

            return(
                    <Link to={`citideleteCityes/${city.name}`} className="search_css" key={city._id}>
                    <img src={city.img} alt="Avatar" />
                     
                      <span className="border">{city.name}</span>
                    </Link> 

                  // con las comillas `` !!! aqui hago una ruta variable segun el nombre de la ciudad
            ) 
          })
    

      return (
       
          <div>
            
            {serchcities}

          </div>
        
      );
    }
  }
   
  const mapStateToProps = (state,ownProps) => {

    let id = ownProps.match.params.city_id;
   
    return {
      cities: state.city.cities.find(city => city._id === id),
      itineraries: state.itinerary.itineraries
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      // deleteCity: (id) => { dispatch(deleteCity(id))},
      deleteCity: (id) => { console.log('here')},
      getItineraries: () => {dispatch(getItineraries())}
    }
  }
  
  export default connect(mapStateToProps , mapDispatchToProps)(CitiesList);
  




  