import React from 'react';
import './Menu.css'
import Button from '@material-ui/core/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {  NavLink } from 'react-router-dom'
import { MdPerson } from "react-icons/md";
import { TiThMenu } from "react-icons/ti";
// import DehazeIcon from '@material-ui/icons/Dehaze';



const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

export default function TemporaryDrawer() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const fullList = side => (
    <div
      className={classes.fullList}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >     
          <List>
          {['Home', 'CITIES', 'Favorites', 'Logout'].map((text, index) => (
            <ListItem button key={text}>
              
              <NavLink to={text.toLowerCase() === 'home' ? '/' : text.toLowerCase()}> <ListItemText primary={text} /></NavLink>
            </ListItem>
          ))}
        </List>
      <Divider />
      
    </div>
  );

  return (
    <div className='menu_top'>

    <NavLink to='./Login'><a><MdPerson className="logo_person" size="1.5rem"/> </a></NavLink>

      <Button  onClick={toggleDrawer('top', true)}><TiThMenu className="logo_menu" size="1.5rem"/>  </Button>

      

      
      <Drawer  anchor="top" open={state.top} onClose={toggleDrawer('top', false)}>
        {fullList('top')}
      </Drawer>

    </div>
  );
}

// 
// 
// <svg xmlns="http://www.w3.org/2000/svg" className="menuicon" width="24" height="24" viewBox="0 0 24 24"><path d="M2 15.5v2h20v-2H2zm0-5v2h20v-2H2zm0-5v2h20v-2H2z"/></svg>